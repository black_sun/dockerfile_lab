FROM python:3.9

WORKDIR /application/main

COPY requirements.txt /application/main/
RUN pip install -r requirements.txt

COPY . /application/main/

EXPOSE 8000
CMD ["gunicorn", "--chdir", "main", "--bind", ":8000", "main.wsgi:application"]